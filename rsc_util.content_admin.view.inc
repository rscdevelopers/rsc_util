<?php

/**
 * @file
 * RSC Content Administration view.
 */

$view = new view();
$view->name = 'content_admin';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Content Admin';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'RSC Content Administration';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access content overview';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['columns'] = array(
  'nid' => 'nid',
  'type' => 'type',
  'title' => 'title',
  'term_node_tid' => 'term_node_tid',
  'field_attachment' => 'field_attachment',
  'field_private_attachment' => 'field_private_attachment',
  'created' => 'created',
  'changed' => 'changed',
  'field_featured' => 'field_featured',
  'status' => 'status',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'nid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'type' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'term_node_tid' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'field_attachment' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'field_private_attachment' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_featured' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Header: Global: Result summary */
$handler->display->display_options['header']['result']['id'] = 'result';
$handler->display->display_options['header']['result']['table'] = 'views';
$handler->display->display_options['header']['result']['field'] = 'result';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['nid']['link_to_node'] = TRUE;
/* Field: Content: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'node';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['element_default_classes'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: All taxonomy terms */
$handler->display->display_options['fields']['term_node_tid']['id'] = 'term_node_tid';
$handler->display->display_options['fields']['term_node_tid']['table'] = 'node';
$handler->display->display_options['fields']['term_node_tid']['field'] = 'term_node_tid';
$handler->display->display_options['fields']['term_node_tid']['label'] = 'Taxonomy terms';
$handler->display->display_options['fields']['term_node_tid']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['term_node_tid']['vocabularies'] = array(
  'categories_general' => 0,
  'cat_word' => 0,
  'cat_study' => 0,
  'tags' => 0,
  'authors' => 0,
  'difficulty' => 0,
  'languages' => 0,
  'newstype' => 0,
  'section' => 0,
  'system' => 0,
  'sources' => 0,
);
/* Field: Private attachment */
$handler->display->display_options['fields']['field_private_attachment']['id'] = 'field_private_attachment';
$handler->display->display_options['fields']['field_private_attachment']['table'] = 'field_data_field_private_attachment';
$handler->display->display_options['fields']['field_private_attachment']['field'] = 'field_private_attachment';
$handler->display->display_options['fields']['field_private_attachment']['ui_name'] = 'Private attachment';
$handler->display->display_options['fields']['field_private_attachment']['label'] = 'Private attachment';
$handler->display->display_options['fields']['field_private_attachment']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_private_attachment']['alter']['text'] = 'link';
$handler->display->display_options['fields']['field_private_attachment']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_private_attachment']['alter']['path'] = '[field_private_attachment]';
$handler->display->display_options['fields']['field_private_attachment']['alter']['external'] = TRUE;
$handler->display->display_options['fields']['field_private_attachment']['alter']['trim_whitespace'] = TRUE;
$handler->display->display_options['fields']['field_private_attachment']['alter']['strip_tags'] = TRUE;
$handler->display->display_options['fields']['field_private_attachment']['element_type'] = '0';
$handler->display->display_options['fields']['field_private_attachment']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_private_attachment']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_private_attachment']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_private_attachment']['type'] = 'file_url_plain';
$handler->display->display_options['fields']['field_private_attachment']['delta_offset'] = '0';
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['created']['date_format'] = 'short';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['changed']['date_format'] = 'short';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['status']['type'] = 'unicode-yes-no';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['text'] = 'Edit';
/* Field: Content: Paragraph */
$handler->display->display_options['fields']['rscc_body']['id'] = 'rscc_body';
$handler->display->display_options['fields']['rscc_body']['table'] = 'field_data_rscc_body';
$handler->display->display_options['fields']['rscc_body']['field'] = 'rscc_body';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  10 => 0,
  9 => 0,
  8 => 0,
);
/* Filter criterion: Has tag */
$handler->display->display_options['filters']['tid']['id'] = 'tid';
$handler->display->display_options['filters']['tid']['table'] = 'taxonomy_index';
$handler->display->display_options['filters']['tid']['field'] = 'tid';
$handler->display->display_options['filters']['tid']['ui_name'] = 'Has tag';
$handler->display->display_options['filters']['tid']['value'] = '';
$handler->display->display_options['filters']['tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['tid']['expose']['operator_id'] = 'tid_op';
$handler->display->display_options['filters']['tid']['expose']['label'] = 'Has tag which ...';
$handler->display->display_options['filters']['tid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['tid']['expose']['operator'] = 'tid_op';
$handler->display->display_options['filters']['tid']['expose']['identifier'] = 'tid';
$handler->display->display_options['filters']['tid']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  7 => 0,
  8 => 0,
  14 => 0,
  13 => 0,
  15 => 0,
);
$handler->display->display_options['filters']['tid']['group_info']['label'] = 'Has taxonomy term';
$handler->display->display_options['filters']['tid']['group_info']['identifier'] = 'tid';
$handler->display->display_options['filters']['tid']['group_info']['remember'] = FALSE;
$handler->display->display_options['filters']['tid']['group_info']['group_items'] = array(
  1 => array(),
  2 => array(),
  3 => array(),
);
$handler->display->display_options['filters']['tid']['vocabulary'] = 'tags';
/* Filter criterion: Content: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['exposed'] = TRUE;
$handler->display->display_options['filters']['nid']['expose']['operator_id'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['label'] = 'Nid';
$handler->display->display_options['filters']['nid']['expose']['operator'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['identifier'] = 'nid';
$handler->display->display_options['filters']['nid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  7 => 0,
  8 => 0,
  14 => 0,
  13 => 0,
  15 => 0,
);
$handler->display->display_options['filters']['nid']['group_info']['label'] = 'Nid';
$handler->display->display_options['filters']['nid']['group_info']['identifier'] = 'nid';
$handler->display->display_options['filters']['nid']['group_info']['remember'] = FALSE;
$handler->display->display_options['filters']['nid']['group_info']['group_items'] = array(
  1 => array(),
  2 => array(),
  3 => array(),
);
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  7 => 0,
  8 => 0,
  14 => 0,
  13 => 0,
  15 => 0,
);
$handler->display->display_options['filters']['title']['group_info']['label'] = 'Title';
$handler->display->display_options['filters']['title']['group_info']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['group_info']['remember'] = FALSE;
$handler->display->display_options['filters']['title']['group_info']['group_items'] = array(
  1 => array(),
  2 => array(),
  3 => array(),
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  7 => 0,
  8 => 0,
  14 => 0,
  13 => 0,
  15 => 0,
);
/* Filter criterion: Content: Post date */
$handler->display->display_options['filters']['created']['id'] = 'created';
$handler->display->display_options['filters']['created']['table'] = 'node';
$handler->display->display_options['filters']['created']['field'] = 'created';
$handler->display->display_options['filters']['created']['exposed'] = TRUE;
$handler->display->display_options['filters']['created']['expose']['operator_id'] = 'created_op';
$handler->display->display_options['filters']['created']['expose']['label'] = 'Post date';
$handler->display->display_options['filters']['created']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['created']['expose']['operator'] = 'created_op';
$handler->display->display_options['filters']['created']['expose']['identifier'] = 'created';
$handler->display->display_options['filters']['created']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  7 => 0,
  8 => 0,
  14 => 0,
  13 => 0,
  15 => 0,
);
/* Filter criterion: Content: Updated date */
$handler->display->display_options['filters']['changed']['id'] = 'changed';
$handler->display->display_options['filters']['changed']['table'] = 'node';
$handler->display->display_options['filters']['changed']['field'] = 'changed';
$handler->display->display_options['filters']['changed']['exposed'] = TRUE;
$handler->display->display_options['filters']['changed']['expose']['operator_id'] = 'changed_op';
$handler->display->display_options['filters']['changed']['expose']['label'] = 'Updated date';
$handler->display->display_options['filters']['changed']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['changed']['expose']['operator'] = 'changed_op';
$handler->display->display_options['filters']['changed']['expose']['identifier'] = 'changed';
$handler->display->display_options['filters']['changed']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  7 => 0,
  8 => 0,
  14 => 0,
  13 => 0,
  15 => 0,
);
/* Filter criterion: Content: Author uid */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'node';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['value'] = '';
$handler->display->display_options['filters']['uid']['exposed'] = TRUE;
$handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['label'] = 'Author uid';
$handler->display->display_options['filters']['uid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
$handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  10 => 0,
  9 => 0,
  8 => 0,
);
/* Filter criterion: Content: Paragraph (rscc_body) */
$handler->display->display_options['filters']['rscc_body_value']['id'] = 'rscc_body_value';
$handler->display->display_options['filters']['rscc_body_value']['table'] = 'field_data_rscc_body';
$handler->display->display_options['filters']['rscc_body_value']['field'] = 'rscc_body_value';
$handler->display->display_options['filters']['rscc_body_value']['operator'] = 'contains';
$handler->display->display_options['filters']['rscc_body_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['rscc_body_value']['expose']['operator_id'] = 'rscc_body_value_op';
$handler->display->display_options['filters']['rscc_body_value']['expose']['label'] = 'Quote (rscc_body)';
$handler->display->display_options['filters']['rscc_body_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['rscc_body_value']['expose']['operator'] = 'rscc_body_value_op';
$handler->display->display_options['filters']['rscc_body_value']['expose']['identifier'] = 'rscc_body_value';
$handler->display->display_options['filters']['rscc_body_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  10 => 0,
  9 => 0,
  8 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
);
/* Filter criterion: Author (Citations) */
$handler->display->display_options['filters']['rscc_author_authors_tid']['id'] = 'rscc_author_authors_tid';
$handler->display->display_options['filters']['rscc_author_authors_tid']['table'] = 'field_data_rscc_author_authors';
$handler->display->display_options['filters']['rscc_author_authors_tid']['field'] = 'rscc_author_authors_tid';
$handler->display->display_options['filters']['rscc_author_authors_tid']['ui_name'] = 'Author (Citations)';
$handler->display->display_options['filters']['rscc_author_authors_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['rscc_author_authors_tid']['expose']['operator_id'] = 'rscc_author_authors_tid_op';
$handler->display->display_options['filters']['rscc_author_authors_tid']['expose']['label'] = 'Author (Citations)';
$handler->display->display_options['filters']['rscc_author_authors_tid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['rscc_author_authors_tid']['expose']['operator'] = 'rscc_author_authors_tid_op';
$handler->display->display_options['filters']['rscc_author_authors_tid']['expose']['identifier'] = 'rscc_author_authors_tid';
$handler->display->display_options['filters']['rscc_author_authors_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  10 => 0,
  9 => 0,
  8 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
);
$handler->display->display_options['filters']['rscc_author_authors_tid']['type'] = 'textfield';
$handler->display->display_options['filters']['rscc_author_authors_tid']['vocabulary'] = 'authors';
/* Filter criterion: Author (Library) */
$handler->display->display_options['filters']['rscl_author_authors_tid']['id'] = 'rscl_author_authors_tid';
$handler->display->display_options['filters']['rscl_author_authors_tid']['table'] = 'field_data_rscl_author_authors';
$handler->display->display_options['filters']['rscl_author_authors_tid']['field'] = 'rscl_author_authors_tid';
$handler->display->display_options['filters']['rscl_author_authors_tid']['ui_name'] = 'Author (Library)';
$handler->display->display_options['filters']['rscl_author_authors_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['rscl_author_authors_tid']['expose']['operator_id'] = 'rscl_author_authors_tid_op';
$handler->display->display_options['filters']['rscl_author_authors_tid']['expose']['label'] = 'Author (Library)';
$handler->display->display_options['filters']['rscl_author_authors_tid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['rscl_author_authors_tid']['expose']['operator'] = 'rscl_author_authors_tid_op';
$handler->display->display_options['filters']['rscl_author_authors_tid']['expose']['identifier'] = 'rscl_author_authors_tid';
$handler->display->display_options['filters']['rscl_author_authors_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  10 => 0,
  9 => 0,
  8 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
);
$handler->display->display_options['filters']['rscl_author_authors_tid']['type'] = 'textfield';
$handler->display->display_options['filters']['rscl_author_authors_tid']['vocabulary'] = 'authors';
/* Filter criterion: Content: Code (rscl_code) */
$handler->display->display_options['filters']['rscl_code_value']['id'] = 'rscl_code_value';
$handler->display->display_options['filters']['rscl_code_value']['table'] = 'field_data_rscl_code';
$handler->display->display_options['filters']['rscl_code_value']['field'] = 'rscl_code_value';
$handler->display->display_options['filters']['rscl_code_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['rscl_code_value']['expose']['operator_id'] = 'rscl_code_value_op';
$handler->display->display_options['filters']['rscl_code_value']['expose']['label'] = 'Code (rscl_code)';
$handler->display->display_options['filters']['rscl_code_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['rscl_code_value']['expose']['operator'] = 'rscl_code_value_op';
$handler->display->display_options['filters']['rscl_code_value']['expose']['identifier'] = 'rscl_code_value';
$handler->display->display_options['filters']['rscl_code_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
  5 => 0,
  6 => 0,
  7 => 0,
  10 => 0,
  9 => 0,
  8 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
  16 => 0,
  17 => 0,
);
/* Filter criterion: Content: Full text (rscl_full_text) */
$handler->display->display_options['filters']['rscl_full_text_value']['id'] = 'rscl_full_text_value';
$handler->display->display_options['filters']['rscl_full_text_value']['table'] = 'field_data_rscl_full_text';
$handler->display->display_options['filters']['rscl_full_text_value']['field'] = 'rscl_full_text_value';
$handler->display->display_options['filters']['rscl_full_text_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['operator_id'] = 'rscl_full_text_value_op';
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['label'] = 'Full text (rscl_full_text)';
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['description'] = 'Full text field on RSC Library content';
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['operator'] = 'rscl_full_text_value_op';
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['identifier'] = 'rscl_full_text_value';
$handler->display->display_options['filters']['rscl_full_text_value']['expose']['remember_roles'] = array(
  2 => '2',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/rsc-content-admin';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'RSC Content Administration';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
