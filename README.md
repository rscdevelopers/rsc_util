# rsc_util
Various utility functions, such as a list of missing files.

## What it does:

- Defines pages to work with files (these were written to help with disaster recovery in 2015): 
    - Missing files � View file fields (a.k.a. attachments) for which one or more files are missing (i.e. not present on the file system).
    - Missing files - suggestions � Try to find suggested matches for missing files based on file names
    - Unmanaged files � View files that are present on the filesystem, but are not referenced by a node.
- Defines a page at `admin/content/rsc_util_acls` to work with ACLs (if the ACL module is enabled). This was done in 2018 to fix problems created by a flexiaccess issue: https://www.drupal.org/project/flexiaccess/issues/2984644
