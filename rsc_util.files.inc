<?php

/**
 * @file
 * File helper functions.
 */

/**
 * Match missing files to unmanaged files.
 *
 * Compare all missing DB files with unmanaged filesystem files, and suggest
 * probable matches between them, based on the levenshtein distance between
 * the filenames.
 */
function rsc_util_get_missing_file_suggestions() {
  // Get all missing files.
  $missing_files = rsc_util_get_missing_files();

  // Get all available unmanaged files.
  $available_files = rsc_util_get_unmanaged_files_recursive();

  // For every missing file, calculate the levenshtein distance from each
  // available file.
  foreach ($missing_files as &$missing_file) {
    $suggestions = [];
    $distances = [];
    foreach ($available_files as &$available_file) {
      $m_filename = $missing_file['file']->uri;
      $a_filename = $available_file->getUri();

      // A quick hack to make RSC's file finding faster, since all our
      // important files start with public://articles/yyyynnnn .
      if (substr($m_filename, 0, 26) == substr($a_filename, 0, 26)) {
        $distances[] = 0;
      }
      else {
        $distances[] = levenshtein($m_filename, $a_filename, 2, 2, 1);
      }

      $suggestions[] = $available_file;
    }
    array_multisort($distances, SORT_ASC, SORT_NUMERIC, $suggestions);
    array_splice($suggestions, 10);
    $missing_file['suggestions'] = $suggestions;
  }

  return $missing_files;
}

/**
 * Update the uri and name of a file in the database.
 */
function rsc_util_change_uri($src_uri, $dst_uri) {
  $n = 0;
  $file = db_select('file_managed', 'f')
    ->fields('f')
    ->condition('uri', $src_uri)
    ->execute()
    ->fetchAssoc();
  if ($file) {

    $dst_filename = explode('/', $dst_uri)[0];

    $q = db_update('file_managed')
      ->fields([
        'uri' => $dst_uri,
        'filename' => $dst_filename,
      ])
      ->condition('fid', $file['fid']);

    $n = $q->execute();

    if ($n != 1) {
      drupal_set_message("Error while updating file with uri {$src_uri} to uri {$dst_uri}", 'error');
    }
  }
  else {
    drupal_set_message("Cannot find file with uri {$src_uri} in database.", 'error');
  }

  return $n;
}

/**
 * Get a list of all files that are not registered in the Drupal DB.
 */
function rsc_util_get_unmanaged_files_recursive($dir_uri = 'public://') {
  $unmanaged_files = [];

  /** @var DrupalPrivateStreamWrapper $dir_wrapper */
  if ($dir_wrapper = file_stream_wrapper_get_instance_by_uri($dir_uri)) {
    $dir_realpath = $dir_wrapper->realpath();

    foreach (scandir($dir_realpath) as $entry) {

      if (substr($entry, 0, 1) != '.') {

        if (substr($dir_uri, -1, 1) != '/') {
          $dir_uri .= '/';
        }
        $entry_uri = $dir_uri . $entry;

        /** @var DrupalPrivateStreamWrapper $entry_wrapper */
        if ($entry_wrapper = file_stream_wrapper_get_instance_by_uri($entry_uri)) {
          $entry_realpath = $entry_wrapper->realpath();

          if (is_dir($entry_realpath)) {
            $unmanaged_files = array_merge($unmanaged_files, rsc_util_get_unmanaged_files_recursive($entry_uri . '/'));
          }
          else {
            // Test whether the file is managed by Drupal.
            $count = db_select('file_managed', 'f')
              ->fields('f')
              ->condition('uri', '%' . db_like($entry), 'LIKE')
              ->execute()
              ->rowCount();
            // TODO: also check this against the field storage
            // (like in rsc_util_missing_files function).
            if ($count == 0) {
              $unmanaged_files[] = $entry_wrapper;
            }
          }
        }

      }

    }

  }
  else {
    drupal_set_message(t('Could not determine public files location'), 'error');
  }

  return $unmanaged_files;
}

/**
 * Check the existence of every file in the database on the filesystem.
 *
 * @return array
 *   A list of missing files.
 */
function rsc_util_get_missing_files() {
  // Get the entire file table.
  $files = db_select('file_managed', 'f')
    ->fields('f')
    ->execute()
    ->fetchAllAssoc('fid');

  // Check if each file exists and add missing files to an array.
  $missing_files = [];
  foreach ($files as &$file) {

    if (empty($file->uri)) {
      // Show an error message with details.
      drupal_set_message(t('File fid=@fid has no uri!', [
        '@fid' => $file->fid,
      ]), 'warning');

      // Next file.
      continue;
    }

    /** @var DrupalPrivateStreamWrapper $wrapper */
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    if (!$wrapper) {
      // Could not load file information.

      // Show an error message with details.
      drupal_set_message(t('Could not get file wrapper for file @fid with uri @uri.', [
        '@uri' => $file->uri,
        '@fid' => $file->fid,
      ]), 'error');

      // Next file.
      continue;
    }

    $path = $wrapper->realpath();

    // Check if the file is missing.
    if (!file_exists($path)) {
      $missing_files[] = [
        'file' => $file,
        'wrapper' => $wrapper,
      ];
    }
  }

  return $missing_files;
}

/**
 * Try to figure out what is using the given managed file.
 *
 * Look in the file_usage table AND search the field storage.
 *
 * @param int $fid
 *   The ID of the managed file to inspect.
 *
 * @return array
 *   An array of human-readable strings, describing where this file is used.
 */
function rsc_util_who_needs_this_file(int $fid) {
  $usage = [];

  $usage_table = rsc_util_get_file_usage_from_table($fid);
  foreach ($usage_table as $module => $type_usage) {
    foreach ($type_usage as $type => $ids) {
      foreach ($ids as $id) {
        if ($module == 'file' && $type == 'node') {
          $node = node_load($id);
          if ($node) {
            $usage[] = t('@node_type node: !node_title', [
              '@node_type' => $node->type,
              '!node_title' => l($node->title, "node/" . $node->nid),
            ]);
          }
          else {
            $usage[] = t('Missing node @nid', [
              '@nid' => $id
            ]);
          }
          continue;
        }

        if ($module == 'user' && $type == 'user') {
          $user = user_load($id);
          if ($user) {
            $usage[] = t('User: !user_name', [
              '!user_name' => l($user->name, "user/" . $user->uid),
            ]);
          } else {
            $usage[] = t('Missing user @uid', [
              '@uid' => $id
            ]);
          }
          continue;
        }

        // Default:
        $usage[] = "$module $type $id";
      }
    }
  }

  $usage_fields = rsc_util_get_fields_using_file($fid);
  foreach ($usage_fields as $field_name => $field_usage) {
    foreach ($field_usage as $entity_type => $ids) {
      foreach ($ids as $id) {
        $usage[] = t("Field: @field_name (in @entity_type @entity_id)", [
          '@field_name' => $field_name,
          '@entity_type' => $entity_type,
          '@entity_id' => $id,
        ]);
      }
    }
  }

  return $usage;
}

/**
 * Delete all field items in file fields referring to the given file ID.
 *
 * @param int $fid
 *   The file ID to search for.
 *
 * @return int
 *   The number of field items deleted.
 *
 * @throws \EntityMetadataWrapperException
 */
function rsc_util_delete_field_items_using_file(int $fid): int {
  // Find all file fields.
  $file_fields = field_read_fields(['type' => 'file']);
  $n = 0;

  // Search all fields for the given file ID.
  foreach ($file_fields as $field_name => $field_info) {
    $query = new EntityFieldQuery();
    $result = $query->fieldCondition($field_name, 'fid', $fid)->execute();
    foreach ($result as $entity_type => $results_of_type) {
      $entities = entity_load($entity_type, array_keys($results_of_type));
      foreach ($entities as $entity) {
        /** @var \EntityDrupalWrapper $wrapper */
        $wrapper = entity_metadata_wrapper($entity_type, $entity);
        /** @var \EntityListWrapper $field_wrapper */
        $field_wrapper = $wrapper->get($field_name);

        $old_field_items = $field_wrapper->value();
        $new_field_items = array_filter($old_field_items, function ($field_item) use ($fid) {
          // If the file ID matches, remove it.
          if ($field_item['fid'] == $fid) {
            return FALSE;
          }

          // Otherwise keep this field item.
          return TRUE;
        });
        $n += count($old_field_items) - count($new_field_items);
        $field_wrapper->set($new_field_items);

        $wrapper->save();
      }
    }
  }

  return $n;
}

/**
 * Search all file fields for the given file ID.
 *
 * @param int $fid
 *   The file ID to search for.
 *
 * @return array
 *   A multi-dimensional array, keyed by field name, then entity type, with
 *   entity IDs at the lowest level.
 */
function rsc_util_get_fields_using_file(int $fid): array {
  // Find all file fields.
  $file_fields = field_read_fields(['type' => 'file']);
  $usage = [];

  // Search all fields for the given file ID.
  foreach ($file_fields as $name => $field) {
    $query = new EntityFieldQuery();
    $result = $query->fieldCondition($name, 'fid', $fid)->execute();
    foreach ($result as $entity_type => $entities) {
      $usage[$name][$entity_type] = array_keys($entities);
    }
  }
  return $usage;
}

/**
 * Search the file_usage table for the given file ID.
 *
 * @param int $fid
 *   The file ID to search for.
 *
 * @return array
 *   A multi-dimensional array, keyed by module, then type, with entity IDs at
 *   the lowest level.
 */
function rsc_util_get_file_usage_from_table(int $fid): array {
  $usage = [];
  /** @var \DatabaseStatementBase $usage_results */
  $usage_results = db_select('file_usage', 'f')
    ->fields('f', [
      'module',
      'type',
      'id',
    ])
    ->condition('fid', $fid)
    ->execute();
  foreach ($usage_results as $ur) {
    $usage[$ur->module][$ur->type][] = $ur->id;
  }
  return $usage;
}
