<?php

/**
 * @file
 * Page callbacks.
 */

module_load_include('inc', 'rsc_util', 'rsc_util.files');

/**
 * Page callback for the file utilities.
 *
 * @return array
 *   Renderable page
 */
function rsc_util_pages_files() {
  return [
    'heading' => [
      '#markup' => t("These pages take long to load, because they query big parts of the database and filesystem. You might have to adjust PHP's maximum execution time."),
    ],
  ];
}

/**
 * Page callback for the missing files utility.
 *
 * @return array
 *   Renderable page
 */
function rsc_util_pages_missing_files() {
  // Get all missing files.
  $missing_files = rsc_util_get_missing_files();

  // Construct a table row for each file.
  $rows = [];
  foreach ($missing_files as $missing_file) {
    $file = $missing_file['file'];
    /** @var DrupalPrivateStreamWrapper $wrapper */
    $wrapper = $missing_file['wrapper'];
    $rows[] = [
      $file->fid,
      $file->filename,
      $wrapper->realpath(),
      implode('<br>', rsc_util_who_needs_this_file($file->fid)),
    ];
  }

  $filenames = array_column($rows, 1);
  array_multisort($filenames, SORT_ASC, SORT_STRING, $rows);

  return [
    'heading' => [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('The following files are registered in the Drupal database, but are missing on the filesystem:'),
    ],
    'table' => [
      '#theme' => 'table',
      '#header' => [
        'fid',
        'filename',
        t('Expected location'),
        t('Required by'),
      ],
      '#rows' => $rows,
    ],
  ];

}


/**
 * Page callback for the unmanaged files utility.
 *
 * @param string $dir
 *   The base folder to start in.
 *
 * @return array
 *   Renderable page
 *
 * @throws \Exception
 */
function rsc_util_pages_unmanaged_files() {
  static $base = "admin/rsc/util/unmanaged";
  $dir_parts = array_filter(func_get_args());

  // Disallow traversing directory tree upwards.
  if (in_array('..', $dir_parts)) {
    throw new Exception("Not allowed to traverse upwards in filesystem.");
  }

  // From http://drupal.stackexchange.com/a/56488/8452
  /** @var DrupalPrivateStreamWrapper $wrapper */
  $wrapper = file_stream_wrapper_get_instance_by_uri('public://' . implode(DIRECTORY_SEPARATOR, $dir_parts));
  if (!$wrapper) {
    throw new Exception('Could not determine public files location.');
  }

  $current_dir = $wrapper->realpath();
  if (!is_dir($current_dir)) {
    drupal_not_found();
    drupal_exit();
  }

  $rows = [];
  foreach (scandir($current_dir) as $sub_dir) {
    // Ignore '.'.
    if ($sub_dir == '.') {
      continue;
    }

    $sub_dir_parts = array_filter(explode(DIRECTORY_SEPARATOR, $sub_dir));
    $entrypath = "{$current_dir}/{$sub_dir}";

    if ($sub_dir == '..') {
      $path_array = array_merge(
        [$base],
        $dir_parts
      );
      array_pop($path_array);
      $rows[] = [
        l($sub_dir, implode(DIRECTORY_SEPARATOR, $path_array)),
        $entrypath,
      ];
    }
    elseif (is_dir($entrypath)) {
      $rows[] = [
        l($sub_dir, implode(DIRECTORY_SEPARATOR, array_merge(
          [$base],
          $dir_parts,
          $sub_dir_parts
        ))),
        $entrypath,
      ];
    }
    else {
      // Test whether the file is managed by Drupal.
      $count = db_select('file_managed', 'f')
        ->fields('f')
        ->condition('uri', '%' . db_like($sub_dir), 'LIKE')
        ->execute()
        ->rowCount();
      if ($count == 0) {
        $rows[] = [$sub_dir, $entrypath];
      }
    }
  }

  return [
    'heading' => [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('The following files are present on the public filesystem, but not referenced in the Drupal database:'),
    ],
    'table' => [
      '#theme' => 'table',
      '#header' => ['name', t('full path')],
      '#rows' => $rows,
    ],
  ];
}

/**
 * Build form for the ACLs utility.
 *
 * @param array $form
 *   The form skeleton.
 * @param array $form_state
 *   The form state.
 * @param int $limit
 *   The maximum number of nodes to list.
 *
 * @return array
 *   Renderable form
 *
 * @throws \Exception
 */
function rsc_util_acls_form(array $form, array &$form_state, $limit = 100) {
  $node_types = node_type_get_types();

  $node_type_options = [];
  foreach ($node_types as $machine_name => $type) {
    $node_type_options[$machine_name] = $type->name;
  }

  // Default values.
  $filter = isset($form_state['values']['filter_fieldset']) ? $form_state['values']['filter_fieldset'] : [
    'node_type' => NULL,
  ];

  $query = db_select('acl_node', 'an')
    ->fields('an', ['nid'])
    ->distinct()
    ->range(0, $limit);
  if (is_array($filter['node_type']) && count($filter['node_type'])) {
    $query->join(
      'node',
      'n',
      'n.nid = an.nid'
    );
    $query->condition('n.type', $filter['node_type'], 'IN');
  }
  $nids = $query->execute()->fetchCol(0);
  $nodes = node_load_multiple($nids);

  $table_header = [
    'nid' => ['data' => t('NID'), 'field' => 'n.nid'],
    'title' => ['data' => t('Title'), 'field' => 'n.title'],
    'type' => ['data' => t('Type'), 'field' => 'n.type'],
    'author' => t('Author'),
    'status' => ['data' => t('Status'), 'field' => 'n.status'],
    'changed' => [
      'data' => t('Updated'),
      'field' => 'n.changed',
      'sort' => 'desc',
    ],
  ];

  $table_options = [];
  foreach ($nodes as $node) {
    $table_options[$node->nid] = [
      'nid' => $node->nid,
      'title' => [
        'data' => [
          '#type' => 'link',
          '#title' => $node->title,
          '#href' => 'node/' . $node->nid,
          '#suffix' => ' ' . theme('mark', ['type' => node_mark($node->nid, $node->changed)]),
        ],
      ],
      'type' => check_plain(node_type_get_name($node)),
      'author' => theme('username', ['account' => $node]),
      'status' => $node->status ? t('published') : t('not published'),
      'changed' => format_date($node->changed, 'short'),
    ];
  }

  return [
    'heading' => [
      '#markup' => t("<p>The following nodes have ACLs on this site. Only the first @n are shown.</p>", [
        '@n' => $limit,
      ]),
    ],
    'filter_fieldset' => [
      '#type' => 'fieldset',
      '#title' => t('Filter nodes ...'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      'node_type' => [
        '#type' => 'select',
        '#title' => t('Content type'),
        '#options' => $node_type_options,
        '#size' => 8,
        '#required' => FALSE,
        '#multiple' => TRUE,
        '#empty_value' => NULL,
        '#default_value' => $filter['node_type'],
      ],
      'filter_button' => [
        '#type' => 'submit',
        '#value' => t('Filter'),
      ],
    ],
    'table' => [
      '#type' => 'tableselect',
      '#header' => $table_header,
      '#options' => $table_options,
      '#empty' => t('There are no ACLs to display.'),
    ],
    'remove_button' => [
      '#type' => 'submit',
      '#value' => t('Remove ACLs for selected nodes.'),
    ],
  ];
}

/**
 * Submit the ACLs form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_acls_form_submit(array $form, array &$form_state) {
  $form_state['rebuild'] = TRUE;

  switch ($form_state['values']['op']) {
    case t('Remove ACLs for selected nodes.'):
      $nids = array_filter($form_state['values']['table']);
      if (empty($nids)) {
        drupal_set_message(t('No nodes were selected. Nothing was removed.'));
      }
      else {

        // Get all ACLs to delete.
        $query = db_select('acl_node', 'an')
          ->fields('an', ['acl_id'])
          ->condition('an.nid', $nids, 'IN');
        $acl_ids = $query->execute()->fetchCol(0);

        // Delete them all at once instead of calling acl_delete_acl repeatedly.
        db_delete('acl')
          ->condition('acl_id', $acl_ids, 'IN')
          ->execute();
        db_delete('acl_user')
          ->condition('acl_id', $acl_ids, 'IN')
          ->execute();
        db_delete('acl_node')
          ->condition('acl_id', $acl_ids, 'IN')
          ->execute();

        drupal_set_message(format_plural(count($acl_ids), 'Deleted 1 ACL.', 'Deleted @count ACLs.'));

        // For the affected nodes...
        $nodes = node_load_multiple($nids);
        $n_deleted = 0;
        foreach ($nodes as $node) {

          // Update the node access records.
          node_access_acquire_grants($node);

          // Forcibly remove cached pages for this node.
          $n_deleted += rsc_util_force_clear_page_cache_for_node($node);

        }

        drupal_set_message(format_plural(
          count($nodes),
          'Updated 1 node access grant.',
          'Updated @count node access grants.'
        ));

        drupal_set_message(format_plural(
          $n_deleted,
          'Cleared 1 page from the cache.',
          'Cleared @count pages from the cache.'
        ));

      }
      break;
  }
}

/**
 * Forcibly clear the page cache.
 *
 * The cache_page table contains separate records for the aliased and unaliased
 * paths of a node. Remove both.
 *
 * @param \stdClass $node
 *   The node for which to clear the cache.
 *
 * @return int
 *   The number of records deleted from the cache_page table.
 */
function rsc_util_force_clear_page_cache_for_node(\stdClass $node) {
  $path = 'node/' . $node->nid;
  $url = url($path);

  $or = db_or();
  $or->condition('cid', "%$path%", 'LIKE');
  $or->condition('cid', "%$url%", 'LIKE');
  $query = db_delete('cache_page')->condition($or);
  $result = $query->execute();
  return $result;
}

/**
 * Build form rsc_util_large_images.
 *
 * @param array $form
 *   The form skeleton.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The renderable form.
 */
function rsc_util_large_images(array $form, array &$form_state) {
  $columns = [
    'fid',
    'uid',
    'filename',
    'uri',
    'filemime',
    'filesize',
    'status',
    'timestamp',
  ];

  $query = db_select('file_managed', 'f')
    ->fields('f', $columns)
    ->condition('filemime', 'image/%', 'LIKE')
    ->orderBy('filesize', 'DESC')
    ->range(0, 100);
  /** @var \DatabaseStatementBase $results */
  $results = $query->execute();

  $options = [];
  foreach ($results as $result) {
    $option = (array) $result;
    $src = file_create_url($result->uri);
    $option['filesize'] = rsc_common_format_filesize($option['filesize']);
    $option['dimensions'] = '<pre>' . print_r(getimagesize($src), TRUE) . '</pre>';
    $option['preview'] = "<a href='$src' target='_blank'><img src='$src' style='max-width: 200px; max-height: 200px;'></a>";
    $option['usage'] = implode('<br>', rsc_util_who_needs_this_file($result->fid));
    $options[$result->fid] = $option;
  }

  $form['table'] = [
    '#type' => 'tableselect',
    '#header' => array_combine($columns, $columns) + [
      'dimensions' => t('Dimensions'),
      'usage' => t('Usage'),
      'preview' => t('Preview'),
    ],
    '#options' => $options,
    '#empty' => t('No images'),
  ];

  $form['max_width'] = [
    '#type' => 'numberfield',
    '#title' => t('Maximum image width'),
    '#description' => t('In pixels'),
    '#step' => 1,
    '#min' => 1,
    '#default_value' => 300,
  ];

  $form['max_height'] = [
    '#type' => 'numberfield',
    '#title' => t('Maximum image height'),
    '#description' => t('In pixels'),
    '#step' => 1,
    '#min' => 1,
    '#default_value' => 150,
  ];

  $form['actions'] = [
    '#type' => 'checkboxes',
    '#title' => t('Actions:'),
    '#options' => [
      'convert' => t('Convert to JPG.'),
      'scale' => t('Scale the image to fit inside the maximum dimensions.'),
    ],
    '#description' => t('Choose an optional action to perform when submitting.'),
    'convert' => ['#description' => t('Make sure you have a backup of the original files, because they will be overwritten.')],
    'scale' => ['#description' => t('Make sure you have a backup of the original files, because they will be overwritten.')],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Validate rsc_util_large_images.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_large_images_validate(array $form, array &$form_state) {

}

/**
 * Submit rsc_util_large_images.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_large_images_submit(array $form, array &$form_state) {
  if (empty($form_state['values'])) {
    drupal_set_message(t("Nothing to do."));
    return;
  }

  $convert = !empty($form_state['values']['actions']['convert']);
  $scale = !empty($form_state['values']['actions']['scale']);
  if (!$convert && !$scale) {
    drupal_set_message(t("Nothing to do."));
    return;
  }

  $max_height = $form_state['values']['max_height'];
  $max_width = $form_state['values']['max_width'];
  $fids = array_filter($form_state['values']['table']);

  foreach ($fids as $fid) {
    $file = file_load($fid);

//    $image = imagecreatefrompng($filePath);
//    $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
//    imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
//    imagealphablending($bg, TRUE);
//    imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
//    imagedestroy($image);
//    $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file
//    imagejpeg($bg, $filePath . ".jpg", $quality);
//    imagedestroy($bg);

    drupal_set_message("TODO: convert and scale image $fid");
  }
}

/**
 * Build form rsc_util_unused_files.
 *
 * @param array $form
 *   The form skeleton.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The renderable form.
 */
function rsc_util_unused_files(array $form, array &$form_state) {
  $limit = 100;

  $fids = db_select('file_managed', 'f')
    ->fields('f', ['fid'])
    ->execute()
    ->fetchCol(0);

  $options = [];
  foreach ($fids as $fid) {
    $usage_table = rsc_util_get_file_usage_from_table($fid);
    $usage_fields = rsc_util_get_fields_using_file($fid);
    if (empty($usage_fields) && empty($usage_table)) {
      $file = file_load($fid);
      $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
      $path = $wrapper->realpath();

      $options[$fid] = (array) $file;
      $options[$fid]['exists'] = file_exists($path) ? t("Present") : t("Missing");
    }
    if (count($options) > $limit) {
      break;
    }
  }

  $form['help'] = [
    '#prefix' => '<p>',
    '#suffix' => '<p>',
    '#markup' => t('The following files are registered in the database, but are not used by any entities or fields. It is probably safe to remove them. Only the first @limit are shown.', [
      '@limit' => $limit,
    ]),
  ];

  $form['table'] = [
    '#type' => 'tableselect',
    '#header' => [
      'fid' => 'fid',
      'uid' => 'uid',
      'filename' => 'filename',
      'uri' => 'uri',
      'filemime' => 'filemime',
      'filesize' => 'filesize',
      'status' => 'status',
      'timestamp' => 'timestamp',
      'exists' => t('On filesystem'),
    ],
    '#options' => $options,
    '#empty' => t('No images'),
  ];

  $form['actions'] = [
    '#type' => 'radios',
    '#title' => t('Actions:'),
    '#options' => [
      'delete' => t("Delete"),
    ],
    '#description' => t('Choose an optional action to perform when submitting.'),
    'delete' => ['#description' => t('Remove the file from the database and from the filesystem.')],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Validate rsc_util_unused_files.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_unused_files_validate(array $form, array &$form_state) {

}

/**
 * Submit rsc_util_unused_files.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_unused_files_submit(array $form, array &$form_state) {
  if (empty($form_state['values'])) {
    drupal_set_message(t("Nothing to do."));
    return;
  }

  switch($form_state['values']['actions']) {
    case "delete":
      $fids = array_filter($form_state['values']['table']);
      foreach ($fids as $fid) {
        $file = file_load($fid);
        // `file_delete` returns TRUE even when the file does not exist in the FS.
        if (file_delete($file)) {
          drupal_set_message(t("Deleted @uri", [
            '@uri' => $file->uri
          ]), 'status');
        } else {
          drupal_set_message(t("Failed to delete @uri", [
            '@uri' => $file->uri
          ]), 'error');
        }
      }
      break;

    default:
      drupal_set_message(t("Nothing to do."));
  }
}

/**
 * Build form rsc_util_duplicate_managed_files.
 *
 * @param array $form
 *   The form skeleton.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The renderable form.
 */
function rsc_util_duplicate_managed_files(array $form, array &$form_state) {
  // Set default values.
  $limit = $form_state['values']['query']['limit'] ?? 50;
  $offset = $form_state['values']['query']['offset'] ?? 0;
  $same_filename = $form_state['values']['query']['same_filename'] ?? TRUE;
  $same_size = $form_state['values']['query']['same_size'] ?? TRUE;

  // Allow user to tweak the query.
  $form['query'] = [
    '#type' => 'fieldset',
    '#title' => 'Query parameters',
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    'limit' => [
      '#type' => 'numberfield',
      '#title' => t('Limit'),
      '#description' => t('The maximum number of results to return (per table). Be careful when increasing this, as it might slow down the server.'),
      '#default_value' => $limit,
      '#step' => 1,
      '#min' => 1,
    ],
    'offset' => [
      '#type' => 'numberfield',
      '#title' => t('Offset'),
      '#description' => t('The number of results to skip over (per table). E.g. when using a limit of 10, an offset of 10 will give you the second page of results, an offset of 20 will give you the third page, etc. Another example: With a limit of 5 and an offset of 3, you will get results 3 through 7 (counting from 0). <a href="@url">Read more about the MySQL LIMIT clause.</a>', [
        '@url' => 'https://www.geeksforgeeks.org/php-mysql-limit-clause/',
      ]),
      '#default_value' => $offset,
      '#step' => 1,
      '#min' => 0,
    ],
    'same_filename' => [
      '#type' => 'checkbox',
      '#title' => t('Group by file name.'),
      '#description' => t('This refers to the file name stored in the DB table, which is not necessarily the same as the last part of the URI'),
      '#default_value' => $same_filename,
    ],
    'same_size' => [
      '#type' => 'checkbox',
      '#title' => t('Group by file size.'),
      '#description' => t('This refers to the file size stored in the DB table, which is not necessarily the same as the size of the file on the disk.'),
      '#default_value' => $same_size,
    ],
    'do_query' => [
      '#type' => 'submit',
      '#value' => t('Reload table'),
      '#submit' => ['rsc_util_duplicate_managed_files__reload_table'],
    ],
  ];

  if (!$same_size && !$same_filename) {
    // We need at least one criterion.
    return $form;
  }

  // Find duplicates based on file names.
  /** @var \SelectQuery $query */
  $query = db_select('file_managed', 'f');
  $query->addExpression('count(*)', 'n');
  if ($same_filename) {
    $query->addField('f', 'filename');
    $query->groupBy('filename');
  }
  if ($same_size) {
    $query->addField('f', 'filesize');
    $query->groupBy('filesize');
  }
  $query->havingCondition('n', 1, '>');
  $query->range($offset, $limit);
  $results = $query->execute()->fetchAll();

  $duplicates = array_map(function ($result) {
    // Get all managed files with this name and/or size.
    $query = db_select('file_managed', 'f')
      ->fields('f', ['fid']);
    if (!empty($result->filename)) {
      $query->condition('filename', $result->filename);
    }
    if (!empty($result->filesize)) {
      $query->condition('filesize', $result->filesize);
    }
    $fids = $query
      ->execute()
      ->fetchCol(0);

    // Make sure we have the File IDs as array keys.
    // `array_map` will preserve them.
    $fids = array_combine($fids, $fids);

    return [
      'common' => (array) $result,
      'items' => array_map(function ($fid) {
        $file = file_load($fid);
        $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
        return [
          'file' => (array) $file,
          'exists' => file_exists($wrapper->realpath()),
          'usage' => rsc_util_who_needs_this_file($fid),
        ];
      }, $fids),
    ];
  }, $results);

  $form['help'] = [
    '#prefix' => '<p>',
    '#suffix' => '<p>',
    '#markup' => t('The following files have the same name and/or size in the database. They might be duplicates.', [
      '@limit' => $limit,
    ]),
  ];

  $form['duplicates'] = array_map(function ($duplicate) {
    $same_filename = !empty($duplicate['common']['filename']);
    $same_filesize = !empty($duplicate['common']['filesize']);

    $heading = [];
    if ($same_filename) {
      $heading[] = $duplicate['common']['filename'];
    }
    if ($same_filesize) {
      $heading[] = $duplicate['common']['filesize'] . " bytes";
    }
    $heading = implode(", ", $heading);

    $header = [
      'fid' => 'fid',
      'uid' => 'uid',
      'uri' => t('Expected location'),
      'filename' => t('File name'),
      'filesize' => t('Size'),
      'filemime' => t('Mime type'),
      'status' => t('Status'),
      'timestamp' => t('Time stamp'),
      'exists' => t('On filesystem'),
      'usage' => t('Required by'),
      'suggestions' => t('Suggestions')
    ];
    if ($same_filename) {
      unset($header['filename']);
    }
    if ($same_filesize) {
      unset($header['filesize']);
    }

    $options = array_map(function ($item) {
      $file = $item['file'];

      $used = count($item['usage']) > 0;
      $exists = $item['exists'];
      if ($used && $exists) {
        // This is normal.
        $suggestion = t("Keep this file.");
      }
      elseif ($used) {
        // Item is in use, but missing.
        $suggestion = t('Required, but missing from the file-system. Visit the "Missing Files - Suggestions" tab.', [
          '@fid' => $item['file']['fid'],
        ]);
      }
      elseif ($exists) {
        // Item exists, but is not in use.
        $suggestion = t('Exists on the file-system, but is not referenced by a field. This is either cruft which can be deleted, or it is an unmanaged file that is referred to from HTML markup somewhere, which we cannot detect easily.', [
          '@fid' => $item['file']['fid'],
        ]);
      }
      else {
        // Item is both missing and unused.
        $suggestion = t('Missing from the file-system and unused. You may probably delete it safely.', [
          '@fid' => $item['file']['fid'],
        ]);
      }

      $extra_columns = [
        'exists' => $item['exists'] ? t('Present') : t('Missing'),
        'usage' => implode("<br>", $item['usage']),
        'suggestions' => $suggestion,
      ];
      return $file + $extra_columns;
    }, $duplicate['items']);

    return [
      'heading' => [
        '#markup' => "<h3>$heading</h3>"
      ],
      'table' => [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $options,
        '#empty' => t('No files'),
      ],
    ];
  }, $duplicates);

  $form['actions'] = [
    '#type' => 'radios',
    '#title' => t('Actions:'),
    '#options' => [
      'delete' => t("Delete"),
      'force_delete' => t("Force delete"),
      'delete_field' => t("Delete field items"),
    ],
    '#description' => t('Choose an optional action to perform when submitting.'),
    'delete_field' => ['#description' => t('Remove all field items that reference the file.')],
    'delete' => ['#description' => t('Remove the file from the database and from the filesystem.')],
    'force_delete' => ['#description' => t('Delete, even if the file is still in use. You should probably never do this.')],
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  $form['#tree'] = TRUE;

  return $form;
}

/**
 * Validate rsc_util_unused_files.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_duplicate_managed_files_validate(array $form, array &$form_state) {

}

/**
 * Submit rsc_util_unused_files.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_duplicate_managed_files_submit(array $form, array &$form_state) {
  if (empty($form_state['values'])) {
    drupal_set_message(t("Nothing to do."));
    return;
  }

  /**
   * Delete multiple files and report results using `drupal_set_message`.
   *
   * @param array $fids
   *   The IDs of the files to delete.
   * @param bool $force
   *   This parameter is passed to `file_delete`.
   */
  function delete_multiple(array $fids, bool $force): void {
    foreach ($fids as $fid) {
      $file = file_load($fid);
      $result = file_delete($file, $force);
      if (is_array($result)) {
        // This means that the deletion was skipped, because the file is in use.
        drupal_set_message(t("File @uri is still in use. Skipped deletion.", [
          '@uri' => $file->uri
        ]), 'status');
      }
      else {
        if ($result) {
          // `file_delete` returns TRUE even when the file does not exist in the FS.
          drupal_set_message(t("Deleted @uri", [
            '@uri' => $file->uri
          ]), 'status');
        }
        else {
          drupal_set_message(t("Failed to delete @uri", [
            '@uri' => $file->uri
          ]), 'error');
        }
      }
    }
  }

  // Get the selected files from each table.
  $fids = array_reduce($form_state['values']['duplicates'], function ($carry, $item) {
    return $carry + array_filter($item['table']);
  }, []);

  switch($form_state['values']['actions']) {
    case "delete_field":
      foreach ($fids as $fid) {
        $file = file_load($fid);
        try {
          $n = rsc_util_delete_field_items_using_file($fid);
          drupal_set_message(format_plural($n, "Deleted one field item that referenced @uri", "Deleted @count field items that referenced @uri", [
            '@uri' => $file->uri,
          ]), 'status');
        } catch (EntityMetadataWrapperException $e) {
          watchdog_exception('rsc_util', $e);
          drupal_set_message(t("Failed to delete field items referencing @uri. @exception", [
            '@uri' => $file->uri,
            '@exception' => $e->getMessage(),
          ]), 'status');
        }
      }
      break;

    case "delete":
      delete_multiple($fids, FALSE);
      break;

    case "force_delete":
      delete_multiple($fids, TRUE);
      break;

    default:
      drupal_set_message(t("Nothing to do."));
  }
}

/**
 * Reload the duplicate managed files table.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function rsc_util_duplicate_managed_files__reload_table(array $form, array &$form_state) {
  $form_state['rebuild'] = TRUE;
}
